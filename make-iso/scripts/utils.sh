#!/bin/bash

SCRIPT_DIR="${1:-/run/install/repo/scripts/}"

# Définition des couleurs
RED='\e[31m'
GREEN='\e[32m'
# shellcheck disable=SC2034
YELLOW='\e[33m'
BLUE='\e[94m'
WHITE='\e[97m'
NC='\e[0m' # No Color

function normalize_yes_no {
    local value
    value="$(echo "$1" | tr '[:upper:]' '[:lower:]')"
    local with_color="${2:-false}"
    if [[ "$value" =~ ^(yes|oui|y|o)$ ]]; then
        ${with_color} && echo -en "${GREEN}"
        echo -n "YES"
        ${with_color} && echo -en "${NC}"
        return 0
    fi
    if [[ "$value" =~ ^(no|non|n)$ ]]; then
        ${with_color} && echo -en "${RED}"
        echo -n "NO"
        ${with_color} && echo -en "${NC}"
        return 1
    fi
}

function validation_no_empty {
    local value="$1"
    if [[ -z "${value}" ]]; then
        echo -e "${RED}Erreur : La valeur ne peut pas être vide.${NC}" >&2
        return 1
    fi
    return 0
}

function validation_yes_no {
    local value="$1"
    if [[ "$(normalize_yes_no "$1")" =~ ^(YES|NO)$ ]]; then
        return 0
    fi
    echo -e "${RED}Erreur : Les valeurs possibles sont : yes, no, oui, non, y, o ou n.${NC}" >&2
    return 1
}

function validation_in_range {
    local value="$1"
    local min="$2"
    local max="$3"

    if ! [[ "$value" =~ ^[0-9]+$ ]] || [ "$value" -lt "$min" ] || [ "$value" -gt "$max" ]; then
        if [[ "$min" -eq "$max" ]]; then
            echo -e "${RED}Erreur : La seule valeur possible est $min.${NC}" >&2
        else
            echo -e "${RED}Erreur : Les valeurs possibles sont comprises entre $min et $max.${NC}" >&2
        fi
        return 1
    fi
    return 0
}

function validation_positive_number {
    local value="$1"

    if ! [[ "$value" =~ ^[0-9]+$ ]] || [ "$value" -lt 0 ]; then
        echo -e "${RED}Erreur : La valeur doit être un nombre positif.${NC}" >&2
        return 1
    fi
    return 0
}

function validation_ipv4 {
    local value="$1"
    local regex="^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$"

    if [[ $value =~ $regex ]]; then
        for ((i=1; i<=4; i++)); do
            if (( BASH_REMATCH[i] > 255 )); then
                echo -e "${RED}Erreur : La valeur doit être une IPv4 valide.${NC}" >&2
                return 1
            fi
        done
        return 0
    else
        echo -e "${RED}Erreur : La valeur doit être une IPv4 valide.${NC}" >&2
        return 1
    fi
}

function ask_user_list {
    local destination_index="$1"
    local title="$2"
    local main_color="$3"
    local secondary_color="$4"
    local list_name="$5"
    local index=1
    local default_index
    default_index="${!destination_index}"

    local list
    mapfile -t list <<< "${!list_name}"

    echo -e "${main_color}Sélectionnez ${title} :"
    echo -e "------------------------${NC}"
    for item in "${!list_name}"; do
        echo -e "${secondary_color}$index${main_color}) $item"
        ((index++))
    done
    local max_value=$((${#list[@]}+1))
    echo -e "------------------------${NC}"
    
    local choice=$((default_index+1))
    ask_user choice "Entrez le numéro de votre choix" "${WHITE}" validation_in_range 1 "${max_value}"
    local target_index=$((choice-1))
    eval "${destination_index}"=\"${target_index}\"
}

function ask_user {
    local destination_var="$1"
    local message="$2"
    local color="$3"
    local validation_function="${4:-true}"
    shift 4
    local validation_params=( "$@" )
    local default_value
    default_value="${!destination_var}"

    local question_end=": "
    if [[ "${message}" =~ \?$ ]]; then
        question_end=""
    fi

    while true; do
        local display_default_value=" "
        if [[ -n "${default_value}" ]]; then
            display_default_value=" [${default_value}] "
        fi
        read -r -p "$(echo -en "${color}${message}${display_default_value}${question_end}${GREEN}")" input
        echo -ne "${NC}"
        if [[ -z "${input}" ]] && [[ -n "${default_value}" ]]; then
            input="${default_value}"
        fi
        if "${validation_function}" "$input" "${validation_params[@]}"; then
            eval "${destination_var}"=\"\$input\"
            return
        fi
    done
}

function ask_password {
    local destination_var="$1"
    local message="$2"
    local color="$3"
    local plaintext="${4:-false}"
    local validation_function="${5:-true}"
    local default_value
    default_value="${!destination_var}"
    if [[ -n "${default_value}" ]]; then
        local choice="non"
        ask_user choice "Un mot de passe pour ${message} est déjà défini, voulez-vous le modifier ?" "${WHITE}" validation_yes_no
        if [[ "$(normalize_yes_no "$choice")" = "NO" ]]; then
            return
        fi
    fi
    while true; do
        read -r -s -p "$(echo -en "${color}Mot de passe de ${message} : ${NC}")" pass
        echo
        if ! "${validation_function}" "$pass"; then
            continue
        fi
        read -r -s -p "$(echo -en "${color}Confirmez le mot de passe de ${message} : ${NC}")" confirm_pass
        echo
        if [[ "$pass" == "$confirm_pass" ]]; then
            if "${plaintext}"; then
                eval "${destination_var}"=\"\$pass\"
                break
            fi
            local hash_password
            hash_password="$(echo -n "${pass}" | "${SCRIPT_DIR}/hash_password.pl")"
            eval "${destination_var}='${hash_password}'"
            break
        else
            echo -e "${RED}Les mots de passe ne correspondent pas. Veuillez réessayer.${NC}"
        fi
    done
}

function show_lists {
    local color="$1"
    local -a header=("${!2}")
    local -a list1=("${!3}")
    local -a list2=("${!4}")
    
    local len1=${#list1[@]}
    local len2=${#list2[@]}
    local max_line=$(( len1 > len2 ? len1 : len2 ))
    
    echo -en "$color"
    printf "+------+----------------+----------------+
"
    printf "| %-4s | %-14s | %-14s |
" "Nb." "${header[0]:- }" "${header[1]:- }"
    printf "+------+----------------+----------------+
"
    for (( i=0; i<max_line; i++ )); do
        printf "| %b%-4s%b | %-14s | %-14s |
" "${BLUE}" "$((i + 1))" "${NC}" "${list1[i]:- }" "${list2[i]:- }"
    done
    printf "+------+----------------+----------------+
"
    echo -en "${NC}"
}

function sum_list {
    local list=("$@")
    local sum=0
    for nb in "${list[@]}"; do
        sum=$((sum + nb))
    done
    echo "$sum"
}

function separator {
    local title="$1"
    local color="$2"
    echo -e "${color}==========${NC}"
    echo -e "${color}==========${WHITE} $title${NC}"
    echo -e "${color}==========${NC}"
}

function handle_no_exit {
    echo ""
    echo -e "${RED}Vous ne pouvez pas quitter ce script...${NC}"
    echo ""
}

function display_category {
    local name="$1"
    echo -en "[${GREEN}${name}${WHITE}]"
}