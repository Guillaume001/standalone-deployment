#!/usr/bin/perl

use strict;
use warnings;

chomp(my $password = <STDIN>);

my $salt = '$6$' . join('', map { ('A'..'Z', 'a'..'z', '0'..'9')[rand 62] } 16);
my $hashed_password = crypt($password, $salt);
print "$hashed_password";