#!/bin/bash

SCRIPT_DIR="$(dirname -- "$0")"

source "${SCRIPT_DIR}/utils.sh" "${SCRIPT_DIR}"

DESTINATION_KS_PART="${1:-/dev/shm/pre.cfg}"

# Associer la fonction handle_no_exit au signal SIGINT
trap 'handle_no_exit' SIGINT

# Autodetect source installation from disk partition or cdrom
device_name="$(findmnt -n -o SOURCE /run/install/repo)"
if lsblk -o NAME,TYPE -lpn | grep "${device_name}" | grep -q rom; then
    LOCATION_SOURCE="cdrom"
    REPOS="repo --name=\"AppStream\" --baseurl=file:///run/install/sources/mount-0000-cdrom/AppStream"
else
    LOCATION_SOURCE="url --url file:///run/install/repo"
    REPOS="repo --name=\"AppStream\" --baseurl=file:///run/install/repo/AppStream"
fi


# Autodetect distribution
if grep -qi "red hat" /run/install/repo/media.repo; then
    DISTRIB="rhel"
elif grep -qi "almalinux" /run/install/repo/media.repo; then
    DISTRIB="almalinux"
elif grep -qi "rocky linux" /run/install/repo/media.repo; then
    DISTRIB="rhel" # why not...
fi
DISTRIB_VERSION_MAJOR="$(grep -iE "^name" /run/install/repo/media.repo | tr -d '[a-zA-Z] =' | cut -d '.' -f 1)"

while true; do
    separator "Début du formulaire de personnalisation" "${YELLOW}"

    # Demander le nom de la machine
    ask_user HOSTNAME "Entrez le nom de la machine" "${WHITE}" validation_no_empty

    # Demander l'identifiant de la machine
    ask_user MACHINE_ID "Entrez l'identifiant de la machine" "${WHITE}" validation_no_empty

    # Demander si l'ordinateur a vocation d'être connecté à un réseau
    OFFLINE_COMPUTER="${OFFLINE_COMPUTER:-non}"
    ask_user OFFLINE_COMPUTER "Est-ce que cette ordinateur sera autonome ?" "${WHITE}" validation_yes_no

    if [[ "$(normalize_yes_no "$OFFLINE_COMPUTER")" = "NO" ]]; then
        USE_SYSLOG="${USE_SYSLOG:-oui}"
    	ask_user USE_SYSLOG "Voulez-vous configurer le client syslog ?" "${WHITE}" validation_yes_no
    	if [[ "$(normalize_yes_no "$USE_SYSLOG")" = "YES" ]]; then
	    	ask_user SYSLOG_COLLECTOR "Veuillez saisir l'adresse du collecteur syslog ?" "${WHITE}" validation_no_empty
	    fi
        USE_NTP="${USE_NTP:-oui}"
        ask_user USE_NTP "Voulez-vous configurer le client NTP ?" "${WHITE}" validation_yes_no
    	if [[ "$(normalize_yes_no "$USE_NTP")" = "YES" ]]; then
            NTP_FROM_DHCP="${NTP_FROM_DHCP:-oui}"
            ask_user NTP_FROM_DHCP "Voulez-vous configurer le client NTP à partir du DHCP ?" "${WHITE}" validation_yes_no
            if [[ "$(normalize_yes_no "$NTP_FROM_DHCP")" = "NO" ]]; then
	    	    ask_user NTP_SRV1 "Veuillez saisir l'adresse du serveur NTP ?" "${WHITE}" validation_no_empty
            fi
	    fi
    fi

    # Demander et chiffrer le mot de passe root
    ask_password HASHED_ROOT_PASS "root" "${WHITE}" false validation_no_empty

    separator "Configuration réseau" "${YELLOW}"

    NET_METHODS=( "DHCP" "static" )
    NET_METHOD_INDEX="${NET_METHOD_INDEX:-0}"
    DNS_FROM_DHCP="${DNS_FROM_DHCP:-oui}"
    ask_user_list NET_METHOD_INDEX "une méthode d'acquisition de la configuration IP ?" "${WHITE}" "${BLUE}" "NET_METHODS[@]"
    NET_METHOD="${NET_METHODS[${NET_METHOD_INDEX}]}"
    if [[ "${NET_METHOD}" = "static" ]]; then
        ask_user NET_IP "Veuillez saisir l'adresse IP" "${WHITE}" validation_ipv4
        ask_user NET_MASK "Veuillez saisir le masque de sous-réseau" "${WHITE}" validation_ipv4
        ask_user NET_GATEWAY "Veuillez saisir l'adresse de passerelle" "${WHITE}" validation_ipv4
    else
        ask_user DNS_FROM_DHCP "Voulez-vous configurer le client DNS à partir du DHCP ?" "${WHITE}" validation_yes_no
    fi
    if [[ "$(normalize_yes_no "$DNS_FROM_DHCP")" = "NO" || "${NET_METHOD}" = "static" ]]; then
        ask_user NET_DNS1 "Veuillez saisir l'adresse du DNS primaire" "${WHITE}" validation_ipv4
        ask_user NET_DNS2 "Veuillez saisir l'adresse du DNS secondaire" "${WHITE}" validation_ipv4
    fi

    separator "Configuration des services" "${YELLOW}"

    # Demander si l'utilisateur veut un serveur sshd
    SERVICE_SSHD="${SERVICE_SSHD:-oui}"
    SSHD_ALLOW_ROOT="${SSHD_ALLOW_ROOT:-non}"
    ask_user SERVICE_SSHD "Voulez-vous un serveur sshd ?" "${WHITE}" validation_yes_no
    if [[ "$(normalize_yes_no "$SERVICE_SSHD")" = "YES" ]]; then
        ask_user SSHD_ALLOW_ROOT "Voulez-vous autoriser la connexion via root sur sshd ?" "${WHITE}" validation_yes_no
    fi

    separator "Configuration des disques" "${YELLOW}"

    # Demander si l'utilisateur veut du chiffrement
    ENCRYPT_DISK="${ENCRYPT_DISK:-non}"
    ask_user ENCRYPT_DISK "Voulez-vous chiffrer le disque ?" "${WHITE}" validation_yes_no
    if [[ "$(normalize_yes_no "$ENCRYPT_DISK")" = "YES" ]]; then
        ask_password PASSWORD_CRYPT_DISK "chiffrement du disque" "${WHITE}" true validation_no_empty
    fi

    # Lister les disques disponibles
    mapfile -t DISKS < <(lsblk -d -n -o NAME,TYPE,SIZE,MOUNTPOINT | grep -v "[SWAP]" | awk '$2 == "disk" {print $1 " => " $3 }')
    DISK_INDEX="${DISK_INDEX:-0}"
    ask_user_list DISK_INDEX "un disque pour l'installation" "${WHITE}" "${BLUE}" "DISKS[@]"
    DISK="$(echo "${DISKS[${DISK_INDEX}]}" | awk '{ print $1 }')"

    # Partitionnement
    # shellcheck disable=SC2034
    HEADER=("Parition" "Taille (Mo)")
    PARTITIONS=("/boot" "/boot/efi" "/" "/usr" "/var" "/var/tmp" "/var/log" "/var/log/audit" "/opt" "/tmp" "/srv" "/home" "swap")
    if ! declare -p "${SIZES}" > /dev/null 2>&1; then
        SIZES=("1024" "600" "8192" "4096" "2048" "4096" "1024" "512" "1024" "1024" "512" "512" "512")
    fi

    function show_parts {
        echo -e "${WHITE}Partitionnement actuel :${NC}"
        show_lists "${WHITE}" HEADER[@] PARTITIONS[@] SIZES[@]
        echo -e "${WHITE}Somme de la taille des parititons : ${YELLOW}$(sum_list "${SIZES[@]}") Mo${NC}"
    }

    show_parts

    # Modifier les tailles des partitions
    MODIFY_PART="${MODIFY_PART:-non}"
    while true; do
        ask_user MODIFY_PART "Voulez-vous modifier la taille d'une partition ?" "${WHITE}" validation_yes_no
        if [[ "$(normalize_yes_no "$MODIFY_PART")" = "NO" ]]; then
            break
        fi
        ask_user PART_INDEX "Choisissez la partition à modifier (numéro)" "${WHITE}" validation_in_range 1 "${#PARTITIONS[@]}"
        NEW_SIZE=${SIZES[PART_INDEX-1]}
        ask_user NEW_SIZE "Nouvelle taille pour ${PARTITIONS[$((PART_INDEX-1))]} (en Mo)" "${WHITE}" validation_positive_number
        SIZES[PART_INDEX-1]=$NEW_SIZE
        show_parts
    done

    separator "Configuration utilisateur" "${YELLOW}"

    # Demander si l'utilisateur veut un environnement graphique
    GUI="${GUI:-non}"
    ask_user GUI "Voulez-vous un environnement graphique ?" "${WHITE}" validation_yes_no

    # Création d'un utilisateur
    CREATE_USER="${CREATE_USER:-non}"
    ask_user CREATE_USER "Voulez-vous créer un utilisateur ?" "${WHITE}" validation_yes_no
    if [[ "$(normalize_yes_no "$CREATE_USER")" = "YES" ]]; then
        ask_user USERNAME "Nom d'utilisateur" "${WHITE}" validation_no_empty
        ask_password HASHED_USER_PASS "de l'utilisation '${USERNAME}'" "${WHITE}" false validation_no_empty
        ask_user SUDO "L'utilisateur '${USERNAME}' doit-il être sudoer ?" "${WHITE}" validation_yes_no
    fi

    separator "Résumé" "$YELLOW"

    # Affiche les options généralistes
    display_category "Générale"
    echo
    echo -e "${WHITE}Nom de la machine : ${BLUE}$HOSTNAME"
    echo -e "${WHITE}Identifiant : ${BLUE}${MACHINE_ID}"
    echo -e "${WHITE}Machine autonome : $(normalize_yes_no "$OFFLINE_COMPUTER" true)"
    if [[ "$(normalize_yes_no "$OFFLINE_COMPUTER")" = "NO" ]]; then
        echo -e "${WHITE}Syslog client : $(normalize_yes_no "$USE_SYSLOG" true)"
        if [[ "$(normalize_yes_no "$USE_SYSLOG")" = "YES" ]]; then
            echo -e "${WHITE}Collecteur syslog : ${BLUE}${SYSLOG_COLLECTOR}"
        fi
        echo -e "${WHITE}NTP client : $(normalize_yes_no "$USE_NTP" true)"
        echo -e "${WHITE}NTP client à partir du DHCP : $(normalize_yes_no "$NTP_FROM_DHCP" true)"
        if [[ "$(normalize_yes_no "$USE_NTP")" = "YES" ]]; then
            echo -e "${WHITE}Serveur NTP : ${BLUE}${NTP_SRV1}"
        fi
    fi

    # Affiche la configuration réseau
    display_category "Réseau"
    echo
    echo -e "${WHITE}Méthode IP : ${BLUE}${NET_METHOD}${NC}"
    if [[ "${NET_METHOD}" = "static" ]]; then
        echo -e "${WHITE}Adresse IP : ${BLUE}${NET_IP}${NC}"
        echo -e "${WHITE}Masque sous réseau : ${BLUE}${NET_MASK}${NC}"
        echo -e "${WHITE}IP de la passerelle : ${BLUE}${NET_GATEWAY}${NC}"
        echo -e "${WHITE}Adresse du DNS primaire : ${BLUE}${NET_DNS1}${NC}"
        echo -e "${WHITE}Adresse du DNS secondaire : ${BLUE}${NET_DNS2}${NC}"
    fi
    if [[ "$(normalize_yes_no "$DNS_FROM_DHCP")" = "NO" ]]; then
        echo -e "${WHITE}Adresse du DNS primaire : ${BLUE}${NET_DNS1}${NC}"
        echo -e "${WHITE}Adresse du DNS secondaire : ${BLUE}${NET_DNS2}${NC}"
    else
        echo -e "${WHITE}Adresse des DNS récupérées depuis DHCP${NC}"
    fi

    # Affiche les services
    display_category "Service"
    echo
    echo -e "${WHITE}Serveur SSHD : $(normalize_yes_no "$SERVICE_SSHD" true)"
    echo -e "${WHITE}Autorise accès root via SSHD : $(normalize_yes_no "$SSHD_ALLOW_ROOT" true)"

    # Affiche les partitions
    display_category "Partition"
    echo
    echo -e "${WHITE}Disque d'installation : ${BLUE}$DISK"
    echo -e "${WHITE}Chiffrement du disque : $(normalize_yes_no "${ENCRYPT_DISK}" true)"
    show_parts

    # Affiche les options pour l'utilisateur
    display_category "Environement"
    echo
    echo -e "${WHITE}Environement graphique : $(normalize_yes_no "$GUI" true)"
    if [[ "$(normalize_yes_no "$CREATE_USER")" = "YES" ]]; then
        echo -e "${WHITE}Utilisateur : ${BLUE}${USERNAME}${NC}"
        echo -e "${WHITE}Ajout des droits sudoer à ${USERNAME} : $(normalize_yes_no "$SUDO" true)"
    else
        echo -e "${WHITE}Utilisateur : $(normalize_yes_no "$CREATE_USER" true)"
    fi

    ask_user VALID "Voulez-vous poursuivre avec les informations précédentes ?" "${RED}" validation_yes_no
    if [[ "$(normalize_yes_no "$VALID")" = "YES" ]]; then
        break
    fi
done


if [[ "$(normalize_yes_no "$ENCRYPT_DISK")" = "YES" ]]; then
    LUKS_LINE="--encrypted --luks-version=luks2 --pbkdf-time=1000 --cipher=aes-xts-plain64 --passphrase=\"${PASSWORD_CRYPT_DISK}\""
else
    LUKS_LINE=""
fi

if [[ "${NET_METHOD}" = "static" ]]; then
    NETWORK_LINE="--ip=${NET_IP} --netmask=${NET_MASK} --gateway=${NET_GATEWAY}"
else
    NETWORK_LINE=""
fi
if [[ "$(normalize_yes_no "DNS_FROM_DHCP")" = "NO" ]]; then
    NETWORK_LINE="${NETWORK_LINE} --nameserver=${NET_DNS1} --nameserver=${NET_DNS2}"
fi

if [[ "$(normalize_yes_no "$SERVICE_SSHD")" = "YES" ]]; then
    SSHD_ENABLE=",sshd"
    SSHD_DISABLE=""
else
    SSHD_ENABLE=""
    SSHD_DISABLE=",sshd"
fi

if [[ "$(normalize_yes_no "$GUI")" = "YES" ]]; then
    GUI_OPT="plymouth-system-theme
plymouth
gnome-tweaks
gnome-shell-extension-*
evince
evince-nautilus
nautilus
firefox
gnome-calculator
gnome-screenshot
gnome-terminal
gnome-terminal-nautilus
gnome-system-monitor
gnome-session-xsession
gnome-backgrounds
gnome-extensions-app
@base-x
@Fonts
@Hardware Support
@GNOME
-PackageKit*
-ModemManager
-NetworkManager-adsl
-avahi
-avahi-tools
-gnome-software
-gnome-initial-setup
-yelp*
-gnome-user-docs
-gnome-logs
-sane-backends-drivers-scanners
-libsane-hpaio
-totem
-gnome-remote-desktop
-fprintd-pam
-cheese
-orca"
else
    GUI_OPT="@^minimal-environment"
fi

if [[ "$(normalize_yes_no "$CREATE_USER")" = "YES" ]]; then
    if [[ "$(normalize_yes_no "$SUDO")" = "YES" ]]; then
        SUDO_LINE="--groups wheel"
    else
        SUDO_LINE=""
    fi
    USER_LINE="user --name=\"${USERNAME}\" --iscrypted --password=\"${HASHED_USER_PASS}\" ${SUDO_LINE}"
else
    USER_LINE=""
fi

PRE_FILES="/dev/shm/pre_to_post/"
POST_ENV_FILE="${PRE_FILES}/env"

mkdir -p "${PRE_FILES}"

# Write pre variables file
cat > "${POST_ENV_FILE}" << EOF
MACHINE_ID="${MACHINE_ID}"
HOSTNAME="${HOSTNAME}"
OFFLINE_COMPUTER="$(normalize_yes_no "${OFFLINE_COMPUTER}")"
SERVICE_SSHD="$(normalize_yes_no "${SERVICE_SSHD}")"
SSHD_ALLOW_ROOT="$(normalize_yes_no "${SSHD_ALLOW_ROOT}")"
GUI="$(normalize_yes_no "${GUI}")"
SYSLOG_COLLECTOR="${SYSLOG_COLLECTOR}"
USE_NTP="$(normalize_yes_no "${USE_NTP}")"
NTP_FROM_DHCP="$(normalize_yes_no "${NTP_FROM_DHCP}")"
NTP_SRV1="${NTP_SRV1}"
DNS_FROM_DHCP="$(normalize_yes_no "${DNS_FROM_DHCP}")"
NET_DNS1="${NET_DNS1}"
NET_DNS2="${NET_DNS2}"
EOF

# Generate part of kickstart included
cat > "${DESTINATION_KS_PART}" << EOF
${REPOS}

${LOCATION_SOURCE}

%addon com_redhat_oscap
    content-type = scap-security-guide
    datastream-id = scap_org.open-scap_datastream_from_xccdf_ssg-${DISTRIB}${DISTRIB_VERSION_MAJOR}-xccdf.xml
    xccdf-id = scap_org.open-scap_cref_ssg-${DISTRIB}${DISTRIB_VERSION_MAJOR}-xccdf.xml
    profile = xccdf_org.ssgproject.content_profile_anssi_bp28_high
%end

services --disabled="${SSHD_DISABLE}" --enabled="nftables${SSHD_ENABLE}"

network --bootproto=${NET_METHOD,,} --hostname="${HOSTNAME}" --noipv6 --onboot=on ${NETWORK_LINE}

%packages --exclude-weakdeps
${GUI_OPT}
aide
audit
chrony
dnf-automatic
logrotate
openscap
openscap-scanner
rsyslog-gnutls
scap-security-guide
sudo
unzip
tar
bzip2
curl
wget
zip
xfsdump
usbutils
man-pages
bash-completion
vim

-dhcp-server
-rsh
-rsh-server
-sendmail
-setroubleshoot
-setroubleshoot-plugins
-setroubleshoot-server
-talk
-talk-server
-telnet
-telnet-server
-tftp
-tftp-server
-xinetd
-ypbind
-ypserv
-subscription-manager
-rhsm*
-rhc*
-iwl*
-avahi
-insights-client
%end

ignoredisk --only-use=$DISK
# Partition clearing information
clearpart --all --initlabel --drives=$DISK
# Disk partitioning information
part /boot --fstype="xfs" --ondisk=$DISK --size=${SIZES[0]} --fsoptions="nosuid,nodev,noexec"
part /boot/efi --fstype="efi" --ondisk=$DISK --size=${SIZES[1]} --fsoptions="nodev,umask=0077,shortname=winnt"
part pv.111 --fstype="lvmpv" --ondisk=$DISK --size=1 --grow ${LUKS_LINE}
volgroup sysvg --pesize=4096 pv.111
logvol / --fstype="xfs" --size=${SIZES[2]} --name=rootlv --vgname=sysvg
logvol /usr --fstype="xfs" --size=${SIZES[3]} --fsoptions="nodev" --name=usrlv --vgname=sysvg
logvol /var --fstype="xfs" --size=${SIZES[4]} --name=varlv --vgname=sysvg
logvol /var/tmp --fstype="xfs" --size=${SIZES[5]} --name=var_tmplv --vgname=sysvg
logvol /var/log --fstype="xfs" --size=${SIZES[6]} --name=var_loglv --vgname=sysvg
logvol /var/log/audit --fstype="xfs" --size=${SIZES[7]} --name=var_log_auditlv --vgname=sysvg
logvol /opt --fstype="xfs" --size=${SIZES[8]} --name=optlv --vgname=sysvg
logvol /tmp --fstype="xfs" --size=${SIZES[9]} --name=tmplv --vgname=sysvg
logvol /srv --fstype="xfs" --size=${SIZES[10]} --name=srvlv --vgname=sysvg
logvol /home --fstype="xfs" --size=${SIZES[11]} --name=homelv --vgname=sysvg
logvol swap --fstype="swap" --size=${SIZES[12]} --name=swaplv --vgname=sysvg

rootpw --iscrypted ${HASHED_ROOT_PASS}

${USER_LINE}

EOF