#!/bin/bash

SCRIPT_DIR="$(dirname -- "$0")"

source "${SCRIPT_DIR}/utils.sh" "${SCRIPT_DIR}"

source /root/install_scripts/env

# Ajout de l'identifiant machine dans hostnamectl
echo "LOCATION=\"${MACHINE_ID}\"" >> /etc/machine-info

if [[ "${GUI}" = "YES" ]]; then
    # theme plymouth
    plymouth-set-default-theme bgrt

    # désactivation de la polyinstanciation du /tmp difficilement géré par gnome
    sed -i "s|^/tmp|#/tmp|g" /etc/security/namespace.conf

    # configuration de gnome
    cat > /etc/dconf/db/local.d/00-gnome-dconf-defaults <<EOF
[org/gnome/desktop/background]
show-desktop-icons=true

[org/gnome/desktop/input-sources]
xkb-options=['terminate:ctrl_alt_bksp']

[org/gnome/desktop/wm/preferences]
button-layout='appmenu:minimize,maximize,close'

[org/gnome/shell]
enabled-extensions=['desktop-icons@gnome-shell-extensions.gcampax.github.com', 'background-logo@fedorahosted.org', 'panel-favorites@gnome-shell-extensions.gcampax.github.com']

[org/gnome/login-screen]
disable-user-list=true
banner-message-enable=true
banner-message-text="Poste Linux - ${HOSTNAME}"

[org/gnome/online-accounts]
whitelisted-providers=['']
EOF
    dconf update

    # définir le graphique par défaut
    systemctl set-default graphical.target
fi

# Modifie l'accès root via SSH
if [[ "${SSHD_ALLOW_ROOT}" = "YES" ]]; then
    sed -i 's/PermitRootLogin no/PermitRootLogin yes/g' /etc/ssh/sshd_config.d/00-complianceascode-hardening.conf
fi

# disable firewalld and use nftables with default configuration
if [[ "${SERVICE_SSHD}" = "YES" ]]; then
    NFTABLES_ALLOW_SSH="tcp dport 22 counter accept"
else
    NFTABLES_ALLOW_SSH=""
fi

cat > /etc/sysconfig/nftables.conf <<EOF
table inet filter {
	chain FORWARD {
		type filter hook forward priority filter; policy drop;
		log prefix "[nftables] FORWARD Rejected: " flags all counter
	}

	chain INPUT {
		type filter hook input priority filter; policy drop;
		iifname "lo" accept
		ct state established,related accept
		ct state invalid drop
		${NFTABLES_ALLOW_SSH}
		log prefix "[nftables] INPUT Rejected: " flags all counter
	}

	chain OUTPUT {
		type filter hook output priority filter; policy accept;
		oifname "lo" accept
		ct state invalid drop
	}
}
EOF

# Remove sssd by default
rm -f /etc/sssd/sssd.conf

# Remove default forwarder put with anssi profile
sed -i '$d' /etc/rsyslog.conf
sed -i '$d' /etc/rsyslog.conf

# Add custom syslog forwarder
if [[ -n "$SYSLOG_COLLECTOR" ]]; then
    echo "*.* action(type=\"omfwd\" target=\"${SYSLOG_COLLECTOR}\" port=\"514\" protocol=\"tcp\")" >> /etc/rsyslog.conf
fi

# Sudo fix permission
chmod 4111 /usr/bin/sudo
# Remove noexec for sudo
sed -i 's/%wheel\tALL=(ALL)\tALL/%wheel\tALL=(ALL)\tEXEC: ALL/g' /etc/sudoers
sed -i 's/root\tALL=(ALL)\tALL/root\tALL=(ALL)\tEXEC: ALL/g' /etc/sudoers

if [[ "${OFFLINE_COMPUTER}" = "YES" ]]; then
    systemctl disable dnf-automatic.timer
fi

# Configuration des yumrepos par défaut
cat > /etc/yum.repos.d/BaseOS.repo <<EOF
[baseos]
name=RedHat \$releasever - BaseOS
baseurl=file:///mnt/distribs/redhat/\$releasever/BaseOS/\$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
EOF

cat > /etc/yum.repos.d/AppStream.repo <<EOF
[appstream]
name=RedHat \$releasever - AppStream
baseurl=file:///mnt/distribs/redhat/\$releasever/AppStream/\$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
EOF

cat > /etc/yum.repos.d/Supplementary.repo <<EOF
[supplementary]
name=RedHat \$releasever - Supplementary
baseurl=file:///mnt/distribs/redhat/\$releasever/Supplementary/\$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
EOF

cat > /etc/yum.repos.d/Codeready-Builder.repo <<EOF
[crb]
name=RedHat \$releasever - Codeready-Builder
baseurl=file:///mnt/distribs/redhat/\$releasever/Codeready-Builder/\$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
EOF

cat > /etc/yum.repos.d/epel.repo <<EOF
[epel]
name=EPEL \$releasever
baseurl=file:///mnt/distribs/epel/\$releasever/Everything/\$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///mnt/distribs/epel/RPM-GPG-KEY-EPEL-9
EOF

# configuration ntp
if [[ "$USE_NTP" = "YES" ]]; then
    if [[ "${NTP_FROM_DHCP}" = "NO" ]]; then
        sed -i 's/^sourcedir/#sourcedir/g' /etc/chrony.conf
    fi
    if [[ -n "${NTP_SRV1}" ]]; then
        sed -i 's/^pool/#pool/g' /etc/chrony.conf
        echo "server ${NTP_SRV1} iburst" >> /etc/chrony.conf
    fi
fi

# mise en place d'un mot de passe grub
grub_password="$(< /dev/urandom tr -dc 'a-zA-Z0-9!@#$%^&*()-_=+[]{}|;:,.<>?/`~' | fold -w 23 | head -n 1)"
grub_hash="$(echo -e "${grub_password}\n${grub_password}\n" | LANG=C grub2-mkpasswd-pbkdf2 | grep PBKDF2 | sed 's/.*is //g')"

echo "set superusers=\"root\"
password_pbkdf2 root ${grub_hash}" >> /etc/grub.d/40_custom

grub2-mkconfig -o /etc/grub2-efi.cfg