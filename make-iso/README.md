# Make your own ISO for Red Hat or its derivatives with customizations during installation

## Overview
This script is designed to create a custom ISO for installing a Red Hat derivative, including personalization during the installation process. It supports various options and parameters to ensure flexibility in usage.

## Usage
To use this script, you can run it with the following command:
```bash
./generate-custom-iso [-h] [-k] [-d] [-w <working_directory>] -i <input.iso> -o <output.iso>
```

### Options
- `-h`, `--help`: Display this help message and exit.
- `-i`, `--input-iso`: Specify the source ISO file.
- `-o`, `--output-iso`: Specify the path and name of the output custom ISO file.
- `-k`, `--keep-workdir`: Keep the working directory after creating the ISO.
- `-w`, `--working-directory`: Specify the working directory. If not provided, a temporary directory will be created.

## Examples
```bash
./generate-custom-iso -h
./generate-custom-iso -i original.iso -o custom.iso
./generate-custom-iso -k -w /path/to/workdir -i original.iso -o custom.iso
```

## Script Functionality
1. **Mounting the Original ISO**: The script mounts the original ISO to a working directory.
2. **Copying the ISO Content**: It copies the content from the mounted ISO to a temporary extraction directory.
3. **Customizing the Installation**: Adds custom GRUB menu entries and includes a kickstart configuration file for personalization during installation.
4. **Building the Custom ISO**: Constructs the final custom ISO using `xorriso`.
5. **Cleanup**: Unmounts the original ISO, optionally deletes the working directory if not specified to be kept.