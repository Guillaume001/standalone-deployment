#!/bin/bash
# ==========================================================
# Nom du fichier : generate-custom-iso
# Auteur         : Guillaume COURS
# Date           : 28/02/2025
# Version        : 1.0
# Description    : Créer une image ISO pour installer une dérivée Red Hat 9 avec personnalisation lors de l'installation
# Usage          : ./generate-custom-iso [-h] [-k] [-d] [-w <working_directory>] -i <input.iso> -o <output.iso>
# ==========================================================

# Activer le mode strict pour la gestion des erreurs
set -uo pipefail

########################################
# Fonctions d'affichage coloré
########################################

# Affiche un message d'information en vert.
log_info() {
  echo -e "\033[1;32m[INFO]\033[0m $1"
}

# Affiche un message d'avertissement en jaune.
log_warning() {
  echo -e "\033[1;33m[WARNING]\033[0m $1"
}

# Affiche un message d'erreur en rouge et envoie sur stderr.
log_error() {
  echo -e "\033[1;31m[ERROR]\033[0m $1" >&2
}

########################################
# Fonction d'affichage de l'aide
########################################

usage() {
  echo "Usage: $0 [-h] [-k] [-d] [-w <working_directory>] -i <input.iso> -o <output.iso>"
  echo ""
  echo "Options:"
  echo "  -h/--help               Affiche ce message d'aide."
  echo "  -i/--input-iso          Spécifier un fichier ISO source."
  echo "  -o/--output-iso         Spécifier le chemin et le nom du fichier de sortie ISO personnalisée."
  echo "  -k/--keep-workdir       Garder le répertoire de travail après la création de l'ISO."
  echo "  -w/--working-directory  Répertoire de travail."
  exit 1
}

########################################
# Vérifier la disponibilité d'une commande
########################################

check_command() {
  if ! command -v "$1" >/dev/null 2>&1; then
    log_error "La commande '$1' est requise mais n'est pas installée."
    exit 1
  fi
}

# Vérifier que les commandes nécessaires sont disponibles
REQUIRED_COMMANDS=("mkdir" "rm" "rsync" "mktemp" "xorriso" "isoinfo" "getopt")
for cmd in "${REQUIRED_COMMANDS[@]}"; do
  check_command "$cmd"
done

########################################
# Traitement des options avec getopts
########################################

# Définir les options courtes et longues
if ! OPTIONS=$(getopt -o hi:o:w:k --long help,input-iso:,output-iso:,working-directory:,keep-workdir -n "$0" -- "$@"); then
  usage
fi

# Réorganiser les paramètres
eval set -- "$OPTIONS"

option_input_iso=""
option_output_iso=""
option_workdir=""
option_keep_workdir=false

while true; do
  case "$1" in
    -h|--help)
      usage
      ;;
    -i|--input-iso)
      option_input_iso="$2"
      shift 2
      ;;
    -o|--output-iso)
      option_output_iso="$2"
      shift 2
      ;;
    -w|--working-directory)
      option_workdir="$2"
      shift 2
      ;;
    -k|--keep-workdir)
      option_keep_workdir=true
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      usage
      ;;
  esac
done

if [ -z "${option_workdir}" ]; then
	option_workdir="$(mktemp --directory)"
fi

if [ -z "${option_keep_workdir}" ]; then
  option_keep_workdir=false
fi

# Vérifier que les options obligatoires ont été fournies
if [ -z "$option_input_iso" ] || [ -z "$option_output_iso" ]; then
  log_error "Les options --input-iso et --output-iso sont obligatoires."
  usage
fi

########################################
# Corps principal du script
########################################

if [ -f "${option_output_iso}" ]; then
   log_info "Suppression du fichier de sortie '${option_output_iso}'"
   rm -f "${option_output_iso}"
fi

log_info "Montage de l'ISO d'origine"
mount_dir="${option_workdir}/original_iso"
mkdir -p "${mount_dir}"

if ! mount | grep -q "${mount_dir}"; then
  mount -o loop "${option_input_iso}" "${mount_dir}"
fi

log_info "Recopie de l'ISO vers un dossier un travail"
extract_dir="${option_workdir}/extract_iso"
mkdir -p "${extract_dir}"
rsync -ah --delete-after "${mount_dir}/" "${extract_dir}"

## Récupération du label de l'ISO d'origine
disk_label="$(isoinfo -i "${option_input_iso}" -d 2>/dev/null | grep 'Volume id' | sed -e 's/[^:]*: //')"

## Récupération du nom de la distribution
media_file="${extract_dir}/media.repo"
distrib_name="$(grep "name=" "${media_file}" | cut -d '=' -f 2)"

## Ajout de l'entrée dans le menu grub
grub_file="${extract_dir}/EFI/BOOT/grub.cfg"
chmod 655 "${grub_file}"

line=$(grep -n "### BEGIN /etc/grub.d/10_linux ###" "${grub_file}" | cut -d : -f 1)
sed -i "${line}isubmenu 'Original menu -->' {" "$grub_file"
echo "}" >> "${grub_file}"

echo "menuentry 'Installation personnalisee de ${distrib_name}' --class fedora --class gnu-linux --class gnu --class os {
        linuxefi /images/pxeboot/vmlinuz inst.stage2=hd:LABEL=${disk_label} inst.ks=hd:LABEL=${disk_label}:/ks_custom.cfg quiet
        initrdefi /images/pxeboot/initrd.img
}" >> "${grub_file}"

chmod 555 "${grub_file}"

## Copie du fichier kickstart
rsync -ah ks_custom.cfg "${extract_dir}/"

## Copie des différents scripts
rsync -ah --delete-after scripts/ "${extract_dir}/scripts/"

## Démontage de l'ISO et nettoyage du dossier de travail
log_info "Démontage de l'ISO d'origine"
umount "${mount_dir}"
if ! ${option_keep_workdir}; then
  log_info "Suppression du dossier de travail"
  rm -rf "${option_workdir:?}/*"
fi

## Construction de l'ISO 
log_info "Construction de l'ISO finale"
xorriso -as mkisofs -v -J -r -o "${option_output_iso}" --volid "${disk_label}" \
  -eltorito-alt-boot -e images/efiboot.img -no-emul-boot \
  -isohybrid-gpt-basdat \
  "${extract_dir}"