#!/bin/bash

FOLDER=netinstall
PATH_NI=/boot/$FOLDER
KS_NAME=kickstart.cfg
ARCH=$(uname -m)

ENTRY_NAME="Network installation"

GREEN="\e[32m"
RED="\e[91m"
RESET="\e[0m"

function check_root() {
  if [ "$(id -u)" != "0" ]; then
    echo -e "[$RED-$RESET] This script must be run as root" 1>&2
    exit 1
  fi
}

function check_requirements() {
  echo -e "[$GREEN+$RESET] Check requirements"

  for cmd in curl dig mkdir cd echo chmod grub2-mkconfig grub2-set-default rm xz cpio find grep head; do
    if ! command -v $cmd &> /dev/null; then
      echo -e "[$RED-$RESET] $cmd is not installed"
      exit 1
    fi
  done
}

function version_lte() {
    [  "$1" = "$(echo -e "$1\n$2" | sort -V | head -n1)" ]
}

function version_lt() {
  if [ "$1" = "$2" ]; then
    return 1
  else
    version_lte "$1" "$2"
  fi
}

function show_parameters() {
  echo -e "\nSummary of the parameters:"
  echo -e "\t* PREFIX MIRROR:\t${PREFIX_MIRROR}"
  echo -e "\t* VERSION:\t\t${RHEL_VERSION}"
  echo -e "\t* FQDN:\t\t\t$FQDN"
  if [ "${STATIC}" = "no" ]; then
    echo -e "\t* BOOTPROTO:\t\tDHCP"
  else
    echo -e "\t* BOOTPROTO:\t\tSTATIC"
    echo -e "\t* IP:\t\t\t$IP/$PREFIX"
    echo -e "\t* GATEWAY:\t\t$GW"
    echo -e "\t* NETMASK:\t\t$NETMASK"
  fi
  echo -e "\t* DNS1:\t\t\t$DNS1"
  echo -e "\t* DNS2:\t\t\t$DNS2"
  echo -e "\t* SSHD PORT:\t\t$SSHD_PORT"
  echo -e "\t* ENCRYPTED DISK:\t$ENCRYPTED_DISK"
  echo -e "\t* DEFAULT ACCOUNT:\t$DEFAULT_ACCOUNT"
  echo -e "\t* SSH KEYS FILE:\t$SSH_KEYS_DEPLOYER_FILE"
}

function create_folder() {
  echo -e "[$GREEN+$RESET] Create $PATH_NI folder"
  mkdir -p $PATH_NI
}

function download() {
  echo -e "[$GREEN+$RESET] Download of vmlinuz and initrd.img on '${PREFIX_MIRROR}' version ${RHEL_VERSION}"

  [ "${ARCH}" = "aarch64" ] && path="images/pxeboot" || path="isolinux"

  curl -# "${PREFIX_MIRROR}/${RHEL_VERSION}/BaseOS/${ARCH}/os/${path}/vmlinuz" -o $PATH_NI/vmlinuz
  curl -# "${PREFIX_MIRROR}/${RHEL_VERSION}/BaseOS/${ARCH}/os/${path}/initrd.img" -o $PATH_NI/initrd.img
}

function create_menu_grub() {

  echo -e "[$GREEN+$RESET] Create menu entry named '$ENTRY_NAME' in grub"
  [ "${STATIC}" = "no" ] && KERNEL_IP=dhcp || KERNEL_IP=$IP::$GW:$NETMASK:eth0:none

  local disklabel
  disklabel=$(LANG=C fdisk -l | grep "Disklabel type" | awk '{ print $3 }')
  [ "${disklabel}" = "gpt" ] && root_part="gpt2" || root_part="msdos1"

  echo "#!/bin/sh
  exec tail -n +3 \$0" > /etc/grub.d/300-netinstall
  cat >> /etc/grub.d/300-netinstall << EOF
menuentry "$ENTRY_NAME" {
    set root=(hd0,$root_part)
    linux   /$FOLDER/vmlinuz net.ip=$KERNEL_IP net.ifnames=0 net.nameserver=$DNS1 noipv6 noshell inst.zram=0 inst.sshd=0 inst.ks=file:/$KS_NAME
    initrd  /$FOLDER/initrd.img
}
EOF

  chmod 755 /etc/grub.d/300-netinstall

  local grub_file="/boot/grub2/grub.cfg"
  if [ -d /sys/firmware/efi ]; then
    grub_file="/etc/grub2-efi.cfg"
  fi
  grub2-mkconfig --output=${grub_file} 2&> /dev/null
  grub2-set-default "$ENTRY_NAME"
}

function create_random_passwords() {
  echo -e "[$GREEN+$RESET] Generate randomly passwords"

  PASSWORD_LUKS=$(openssl rand -base64 32)
  PASSWORD_DEPLOYER=$(openssl rand -base64 32)
}

function encrypted_passwords(){
  PASSWORD_DEPLOYER_SALT=$(openssl rand -base64 3)
  PASSWORD_DEPLOYER_ENCRYPTED=$(openssl passwd -6 -salt "${PASSWORD_DEPLOYER_SALT}" "${PASSWORD_DEPLOYER}")
}

function show_passwords() {
  echo -e "------------------------------$RED"
  echo -e "WARNING$RESET: please take note of following passwords: ";
  [ "${ENCRYPTED_DISK}" = "yes" ] && echo -e "LUKS:\t\t$PASSWORD_LUKS"
  echo -e "$DEFAULT_ACCOUNT:\t$PASSWORD_DEPLOYER"
  echo "------------------------------"
}

function create_kickstart() {
  local destination_file="$1"
  local silent="$2"

  [ -z "${silent}" ] && silent="no"

  [ "${silent}" = "no" ] && echo -e "[$GREEN+$RESET] Create KickStart file in ${destination_file}"

  [ "${STATIC}" = "no" ] && KS_IP="--bootproto=dhcp" || KS_IP="--bootproto=static --ip=$IP --netmask=$NETMASK --gateway=$GW"

  [ -b /dev/vda ] && DRIVE_TYPE=vda
  [ -b /dev/sda ] && DRIVE_TYPE=sda
  [ -b /dev/nvme0n1 ] && DRIVE_TYPE=nvme0n1
  [ "${ENCRYPTED_DISK}" = "yes" ] && LUKS_OPTS="--encrypted  --passphrase=${PASSWORD_LUKS}"

  # Workaround to avoid deprecated warning in RHEL 9
  local timezone_option
  timezone_option="--isUtc"
  if version_lte "9" "${RHEL_VERSION}"; then
    timezone_option="--utc"
  fi

  cat > "${destination_file}" << SETUP_KICKSTART
#  Author: Guill4aume001
#   Desc.: Minimal installation for server
#      OS: RHEL and derivatives >= 8
# Version: 1.6

# Turning on text-mode installation (little quicker than GUI)
text

# Do not configure the X Window System
skipx

# Setting up authentication and keyboard
authselect --enableshadow --passalgo=sha512
keyboard --vckeymap=fr --xlayouts='fr'

# Use network installation
url --url="${PREFIX_MIRROR}/${RHEL_VERSION}/BaseOS/${ARCH}/os/"

# Setup repository
repo --name="BaseOS" --baseurl=${PREFIX_MIRROR}/${RHEL_VERSION}/BaseOS/${ARCH}/os/
repo --name="AppStream" --baseurl=${PREFIX_MIRROR}/${RHEL_VERSION}/AppStream/${ARCH}/os/

# Setting up language to French
lang fr-FR.UTF-8

# Setting up network interface to DHCP
network --device=eth0 $KS_IP --noipv6 --hostname=$FQDN --nameserver=$DNS1,$DNS2 --activate

# Using only primary disk, ignoring others
ignoredisk --only-use=$DRIVE_TYPE

# Root password
rootpw --iscrypted changepassword

# Setting timezone
timezone Europe/Paris ${timezone_option}

# System services
services --enabled="NetworkManager,sshd,rsyslog,chronyd,iptables,ip6tables"

# Setting up Security-Enhanced Linux into enforcing
selinux --enforcing

# Setting up MBR
bootloader --append="KEYBOARDTYPE=pc KEYTABLE=fr-latin9 rd.neednet=1 net.ifnames=0 crashkernel=auto quiet" --timeout=3

# Setting partions
# Remove MBR
zerombr
# Setting up Logical Volume Manager and autopartitioning
clearpart --all --drives=$DRIVE_TYPE --initlabel
# Disk partitioning information
part /boot/efi  --fstype=efi   --grow    --maxsize=50           --size=20
part /boot      --fstype=ext4  --fsoptions=nosuid,nodev,noexec  --size=1024
part pv.01      --size=100     --grow    --ondisk=$DRIVE_TYPE  $LUKS_OPTS
volgroup sysvg pv.01
logvol  /         --vgname=sysvg  --fstype=xfs  --fsoptions=                     --size=2048  --name=lv_root
logvol  swap      --vgname=sysvg  --fstype=swap                                  --size=2048  --name=lv_swap
logvol  /opt      --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev         --size=2048  --name=lv_opt
logvol  /tmp      --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev,noexec  --size=1024  --name=lv_tmp
logvol  /srv      --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev         --size=2048  --name=lv_srv
logvol  /home     --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev,noexec  --size=1024  --name=lv_home
#logvol  /proc    --vgname=sysvg  --fstype=xfs  --fsoptions=hidepid=2            --size=1024  --name=lv_proc
logvol  /usr      --vgname=sysvg  --fstype=xfs  --fsoptions=nodev                --size=4096  --name=lv_usr
logvol  /var      --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev,noexec  --size=2048  --name=lv_var
logvol  /var/log  --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev,noexec  --size=1024  --name=lv_var_log
logvol  /var/tmp  --vgname=sysvg  --fstype=xfs  --fsoptions=nosuid,nodev,noexec  --size=1024  --name=lv_var_tmp

# Eject cdrom and reboot
reboot --eject

# Create deployer user
user --name=$DEFAULT_ACCOUNT --iscrypted --password=$PASSWORD_DEPLOYER_ENCRYPTED

# Installing only packages for minimal install
%packages
@core
qemu-guest-agent
langpacks-fr
iptables
iptables-services
policycoreutils-python-utils
-NetworkManager-tui
-firewalld
-iprutils
-iwl*-firmware
-plymouth
%end

%addon com_redhat_kdump --disable
%end

%post

# Disable root account
passwd -d root
passwd -l root

echo "virtual-guest" > /etc/tuned/active_profile

# generic localhost names
cat > /etc/hosts << EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

EOF

# Setup firewall

cat > /etc/sysconfig/iptables << EOF
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport $SSHD_PORT -j ACCEPT
COMMIT
EOF

cat > /etc/sysconfig/ip6tables << EOF
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]
COMMIT
EOF

# Setup SSH server

cat > /etc/ssh/sshd_config << EOF
Protocol 2
Port $SSHD_PORT
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
SyslogFacility AUTHPRIV
LogLevel INFO
PermitRootLogin no
MaxAuthTries 3
AuthorizedKeysFile .ssh/authorized_keys
HostbasedAuthentication no
IgnoreRhosts yes
PasswordAuthentication no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
KerberosAuthentication no
GSSAPIAuthentication no
UsePAM yes
X11Forwarding no
PrintMotd no
PermitUserEnvironment no
ClientAliveInterval 300
ClientAliveCountMax 3
AllowAgentForwarding no
AllowTcpForwarding no
PermitTunnel no
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
Subsystem sftp /usr/libexec/openssh/sftp-server
Ciphers aes128-ctr,aes192-ctr,aes256-ctr
MACs hmac-sha2-512
EOF
semanage port -a -t ssh_port_t -p tcp $SSHD_PORT

# Setup SSH key
mkdir -m700 /home/$DEFAULT_ACCOUNT/.ssh

cat <<EOF >/home/$DEFAULT_ACCOUNT/.ssh/authorized_keys
$(cat "${SSH_KEYS_DEPLOYER_FILE}")
EOF

chmod 0600 /home/$DEFAULT_ACCOUNT/.ssh/authorized_keys
chown $DEFAULT_ACCOUNT:$DEFAULT_ACCOUNT -R /home/$DEFAULT_ACCOUNT/.ssh
restorecon -R /home/$DEFAULT_ACCOUNT/.ssh

# Sudo
echo "$DEFAULT_ACCOUNT ALL=(ALL)    ALL" >> /etc/sudoers

%end
SETUP_KICKSTART
}

function add_kickstart_in_initrd() {
  echo -e "[$GREEN+$RESET] Decompress initrd downloaded"
  rm -rf /tmp/tmp_initrd
  mkdir -p /tmp/tmp_initrd
  cd /tmp/tmp_initrd || exit 1
  xz -dc < $PATH_NI/initrd.img | cpio -idm
  rm -rf $PATH_NI/initrd.img

  echo -e "[$GREEN+$RESET] Add $KS_NAME in initrd"
  cp $PATH_NI/$KS_NAME /tmp/tmp_initrd/$KS_NAME

  echo -e "[$GREEN+$RESET] Compress new initrd"
  find . 2>/dev/null | cpio -c -o | xz -9 --format=lzma > $PATH_NI/initrd.img
  rm -rf /tmp/tmp_initrd
}

function help() {
  echo "Setup Network Installation:"
  echo "Usage: $0 [--show-kickstart]"
  echo ""
  echo "Some of the options include:"
  echo -e "  --show-kickstart\tshow kickstart file only"
  echo -e "  -e, --env-file\tload environment variables from file (default: ./default-env.sh)"
  echo -e "  -n, --no-prompt\tuse default env values without prompt"
  echo -e "  -h, --help\t\tdisplay this help and exit"
}

function load_passwords() {
  PASSWORD_DEPLOYER=$(grep "^$DEFAULT_ACCOUNT:" "${PASSWD_FILE}" | awk -F ':' '{ print $2}')
  [ "${ENCRYPTED_DISK}" = "yes" ] && PASSWORD_LUKS=$(grep "^luks:" "${PASSWD_FILE}" | awk -F ':' '{ print $2}') || PASSWORD_LUKS="no"

  [ -z "$PASSWORD_DEPLOYER" ] || [ -z "$PASSWORD_LUKS" ] && echo -e "[$RED-$RESET] Check the contents of the ${PASSWD_FILE} file" && exit 1
}

function prompt_menu() {
  local prompt="$1"

  if [ "${prompt}" != "true" ]; then
    echo "===================================================="
    echo "====        Setuping network installation       ===="
    echo "===================================================="

    read -r -p "Which mirror do you want [${DEFAULT_PREFIX_MIRROR}]: " PREFIX_MIRROR
    read -r -p "Which version do you want [${DEFAULT_RHEL_VERSION}]: " RHEL_VERSION
    read -r -p "FQDN [${DEFAULT_FQDN}]: " FQDN
    read -r -p "Do you want to setup static IP (yes|no) [$DEFAULT_STATIC]: " STATIC

    if [ "${STATIC}" = "yes" ]; then
      read -r -p "IP [$DEFAULT_IP]: " IP
      read -r -p "Gateway [$DEFAULT_GW]: " GW
      read -r -p "Netmask [$DEFAULT_NETMASK]: " NETMASK
      read -r -p "Prefix [$DEFAULT_PREFIX]: " PREFIX
    fi

    read -r -p "DNS 1 [$DEFAULT_DNS1]: " DNS1
    read -r -p "DNS 2 [$DEFAULT_DNS2]: " DNS2

    read -r -p "Do you want to change the SSH port [$DEFAULT_SSHD_PORT]: " SSHD_PORT
    read -r -p "Do you want to encrypt your disk [$DEFAULT_ENCRYPTED_DISK]: " ENCRYPTED_DISK

    read -r -p "Username of the first account [$DEFAULT_DEFAULT_ACCOUNT]: " DEFAULT_ACCOUNT
    read -r -p "Which file contains the SSH public keys for the default account [$DEFAULT_SSH_KEYS_DEPLOYER_FILE]: " SSH_KEYS_DEPLOYER_FILE
    read -r -p "Which file contains the password of first account (and luks) [$DEFAULT_PASSWD_FILE]: " PASSWD_FILE
  fi

  PREFIX_MIRROR=${PREFIX_MIRROR:-$DEFAULT_PREFIX_MIRROR}
  RHEL_VERSION=${RHEL_VERSION:-$DEFAULT_RHEL_VERSION}
  FQDN=${FQDN:-$DEFAULT_FQDN}
  STATIC=${STATIC:-$DEFAULT_STATIC}
  IP=${IP:-$DEFAULT_IP}
  GW=${GW:-$DEFAULT_GW}
  NETMASK=${NETMASK:-$DEFAULT_NETMASK}
  PREFIX=${PREFIX:-$DEFAULT_PREFIX}
  DNS1=${DNS1:-$DEFAULT_DNS1}
  DNS2=${DNS2:-$DEFAULT_DNS2}
  SSHD_PORT=${SSHD_PORT:-$DEFAULT_SSHD_PORT}
  ENCRYPTED_DISK=${ENCRYPTED_DISK:-$DEFAULT_ENCRYPTED_DISK}
  DEFAULT_ACCOUNT=${DEFAULT_ACCOUNT:-$DEFAULT_DEFAULT_ACCOUNT}
  SSH_KEYS_DEPLOYER_FILE=${SSH_KEYS_DEPLOYER_FILE:-$DEFAULT_SSH_KEYS_DEPLOYER_FILE}
  PASSWD_FILE=${PASSWD_FILE:-$DEFAULT_PASSWD_FILE}
}

function validation_parameters() {
  if [ "${STATIC}" != "yes" ] && [ "${STATIC}" != "no" ]; then
    echo -e "[$RED-$RESET] The value of the STATIC parameter must be yes or no"
    exit 1
  fi

  if [ "${ENCRYPTED_DISK}" != "yes" ] && [ "${ENCRYPTED_DISK}" != "no" ]; then
    echo -e "[$RED-$RESET] The value of the ENCRYPTED_DISK parameter must be yes or no"
    exit 1
  fi

  # Check if PREFIX_MIRROR is valid URL
  if ! curl -s --head "${PREFIX_MIRROR}" | head -n 1 | grep "200" > /dev/null; then
    echo -e "[$RED-$RESET] The value of the PREFIX_MIRROR parameter is not a valid URL"
    exit 1
  fi
  # Remove last / if exists
  PREFIX_MIRROR=${PREFIX_MIRROR%/}

  # Check if IP, GW, NETMASK, DNS1, DNS2 and PREFIX is valid
  if [ "${STATIC}" = "yes" ]; then
    for ip in "${IP}" "${GW}" "${NETMASK}" "${DNS1}" "${DNS2}"; do
      if ! echo "${ip}" | grep -E "^([0-9]{1,3}[\.]){3}[0-9]{1,3}$" > /dev/null; then
        echo -e "[$RED-$RESET] The value of the ${ip} parameter is not a valid IP address"
        exit 1
      fi
    done
    if ! echo "${PREFIX}" | grep -E "^[0-9]{1,2}$" > /dev/null; then
      echo -e "[$RED-$RESET] The value of the PREFIX parameter is not a valid prefix"
      exit 1
    fi
  fi

  # Checks if the DNS server responds to queries
  for dns in "${DNS1}" "${DNS2}"; do
    if ! dig +short "${dns}" > /dev/null; then
      echo -e "[$RED-$RESET] The value of the ${dns} parameter is not a valid DNS server"
      exit 1
    fi
  done
}

function main() {
  check_requirements
  if [ "${show_kickstart}" = "true" ]; then
    prompt_menu "${no_prompt}"
    validation_parameters
    create_kickstart "/dev/stdout" "yes"
    exit 0
  fi
  check_root
  prompt_menu "${no_prompt}"
  validation_parameters
  show_parameters
  create_folder
  download
  create_menu_grub
  if [ -f "${PASSWD_FILE}" ]; then
    load_passwords
  else
    create_random_passwords
  fi
  encrypted_passwords
  show_passwords
  create_kickstart "$PATH_NI/$KS_NAME"
  add_kickstart_in_initrd
}

# Default values
show_kickstart="false"
no_prompt="false"

# Parse parameters
while [[ "$#" -gt 0 ]]; do
  case $1 in
    -k|--show-kickstart) show_kickstart="true" ;;
    -e|--env-file) env_file="$2"; shift 2;;
    -n|--no-prompt) no_prompt="true" ;;
    -h|--help) help; exit 0 ;;
    *) echo "Unknown parameter passed: $1"; exit 1 ;;
  esac
  shift
done

# Source env file
env_file=${env_file:-"./default-env.sh"}
if [ -f "$env_file" ]; then
  # shellcheck source=./default-env.sh
  source "$env_file"
else
  echo -e "[${RED}x${RESET}] $env_file env file not found"
  exit 1
fi

main