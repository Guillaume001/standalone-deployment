# Self reinstallation of RHEL and derivatives using remote mirror

## Introduction

This project aims to provide a simple way to install a RHEL based system with a minimal set of packages and a network configuration that allows to install the system remotely.

## Use case

When you need to reinstall a system remotely and you don't have access to the console, like with VPS in the cloud.

## Usage

Show help:

    $ ./setup_net_install.sh --help

Example:

    $ ./setup_net_install.sh
    [+] Check requirements
    ====================================================
    ====        Setuping network installation       ====
    ====================================================
    Which mirror do you want [https://repo.almalinux.org/almalinux/]:
    Which version do you want [9.1]:
    FQDN [server.example.com]:
    Do you want to setup static IP (yes|no) [no]:
    DNS 1 [1.1.1.1]:
    DNS 2 [1.0.0.1]:
    Do you want to change the SSH port [22]:
    Do you want to encrypt your disk [no]:
    Username of the first account [deployer]:
    Which file contains the SSH public keys for the default account [authorized_keys]:
    Which file contains the password of first account (and luks) [.passwd]:

    Summary of the parameters:
        * PREFIX MIRROR:	https://repo.almalinux.org/almalinux
        * VERSION:	9.1
        * FQDN:		server.example.com
        * BOOTPROTO:	DHCP
        * DNS1:		1.1.1.1
        * DNS2:		1.0.0.1
        * SSHD PORT:	22
        * ENCRYPTED DISK:	no
        * DEFAULT ACCOUNT:	deployer
        * SSH KEYS FILE:	authorized_keys
    [+] Create /boot/netinstall folder
    [+] Download of vmlinuz and initrd.img on 'https://repo.almalinux.org/almalinux' version 9.1
    ################################################################################################# 100.0%
    ################################################################################################# 100.0%
    [+] Create menu entry named 'Network installation' in grub
    ------------------------------
    WARNING: please take note of following passwords:
    deployer:	azerty
    ------------------------------
    [+] Create KickStart file in /boot/netinstall/kickstart.cfg
    [+] Decompress initrd downloaded
    333896 blocks
    [+] Add kickstart.cfg in initrd
    [+] Compress new initrd
    333907 blocks

## Roadmap

- [ ] Using nftables as default firewall
- [ ] Add ssh server at boot to enter the LUKS passphrase
